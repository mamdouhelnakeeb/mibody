package com.mibody.app.helper;

import android.view.View;

/**
 * Created by nakeebimac on 10/18/16.
 */

public interface OnItemClickListener {
    public void onClick(View view, int position);
}